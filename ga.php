<?php

//file_put_contents('./log', 'LOG ' , FILE_APPEND);


// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
//  error_reporting(E_ALL);

require('vendor/autoload.php');
use TheIconic\Tracking\GoogleAnalytics\Analytics;

//file_put_contents('./log', 'LOG ' , FILE_APPEND);


date_default_timezone_set('America/Toronto');

$jsonStr = file_get_contents("php://input");
$json = json_decode($jsonStr);
$ip = $json->data->ip;
$order = $json->data->order;


//AGGREGATED EVENT


$analytics = new Analytics(true);
$analytics
    ->setProtocolVersion('1')
    ->setTrackingId('UA-30967195-4')
    ->setClientId('12345678')
    ->setApplicationName('App')
    ->setIpOverride($ip);
// $in = $analytics->sendPageview();


// Then, include the transaction data
$analytics->setTransactionId(date('Ymd') . '-' . $order->Order->invoice)
    ->setAffiliation('App')
    ->setApplicationName('App')
    ->setRevenue($order->Order->data->Total->subtotal - $order->Order->data->Total->discounts)
    ->setTax($order->Order->data->Total->taxes )
    ->setShipping($order->Order->data->Total->delivery );
    // ->setCouponCode('MY_COUPON');


$x = 1;
foreach ($order->Order->data->Product as $item) {

        $productData = [
            'sku' => $item->Parent->sku,
            'name' => $item->Parent->name,
            // 'brand' => 'Test Brand 2',
            'category' => !empty($item->Parent->category) ? $item->Parent->category : '',
            // 'variant' => 'yellow',
            'price' => $item->Parent->price,
            'quantity' => $item->Parent->qty,
            // 'coupon_code' => 'TEST 2',
            'position' => $x
        ];
        $x++;
        $analytics->addProduct($productData);
        if (!empty($item->Children)){
            foreach ($item->Children as $child){
                $productData = [
                    'sku' => $child->sku,
                    'name' => $child->name,
                    // 'brand' => 'Test Brand 2',
                    'category' => !empty($child->category) ? $child->category : '',
                    // 'variant' => 'yellow',
                    'price' => $child->price,
                    'quantity' => $item->Parent->qty,
                    // 'coupon_code' => 'TEST 2',
                    'position' => $x
                ];
                $x++;
                $analytics->addProduct($productData);  
            }
        }


}


// Don't forget to set the product action, in this case to PURCHASE
$analytics->setProductActionToPurchase();

// Finally, you must send a hit, in this case we send an Event
$analytics->setEventCategory('Checkout')
    ->setEventAction('Purchase')
    ->sendEvent();


//APP ONLY EVENT



$analytics = new Analytics(true);
$analytics
    ->setProtocolVersion('1')
    ->setTrackingId('UA-117556543-1')
    ->setClientId('12345678')
    ->setApplicationName('App')
    ->setIpOverride($ip);
// $in = $analytics->sendPageview();


// Then, include the transaction data
$analytics->setTransactionId(date('Ymd') . '-' . $order->Order->invoice)
    ->setAffiliation('App')
    ->setApplicationName('App')
    ->setRevenue($order->Order->data->Total->subtotal - $order->Order->data->Total->discounts)
    ->setTax($order->Order->data->Total->taxes )
    ->setShipping($order->Order->data->Total->delivery );
    // ->setCouponCode('MY_COUPON');


$x = 1;
foreach ($order->Order->data->Product as $item) {

        $productData = [
            'sku' => $item->Parent->sku,
            'name' => $item->Parent->name,
            // 'brand' => 'Test Brand 2',
            'category' => !empty($item->Parent->category) ? $item->Parent->category : '',
            // 'variant' => 'yellow',
            'price' => $item->Parent->price,
            'quantity' => $item->Parent->qty,
            // 'coupon_code' => 'TEST 2',
            'position' => $x
        ];
        $x++;
        $analytics->addProduct($productData);
        if (!empty($item->Children)){
            foreach ($item->Children as $child){
                $productData = [
                    'sku' => $child->sku,
                    'name' => $child->name,
                    // 'brand' => 'Test Brand 2',
                    'category' => !empty($child->category) ? $child->category : '',
                    // 'variant' => 'yellow',
                    'price' => $child->price,
                    'quantity' => $item->Parent->qty,
                    // 'coupon_code' => 'TEST 2',
                    'position' => $x
                ];
                $x++;
                $analytics->addProduct($productData);  
            }
        }


}


// Don't forget to set the product action, in this case to PURCHASE
$analytics->setProductActionToPurchase();

// Finally, you must send a hit, in this case we send an Event
$res = $analytics->setEventCategory('Checkout')
    ->setDebug(true)
    ->setEventAction('Purchase')
    ->sendEvent();



$out = Array('success' => 1, 'res' => $res->getDebugResponse(), 'error' => '');

echo json_encode($out);


//file_put_contents('./log', 'LOG ' .  $order->Order->invoice . "\r\n"  , FILE_APPEND);


?>
